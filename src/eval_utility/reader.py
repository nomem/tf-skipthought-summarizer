from os import listdir
from os.path import isfile, join
from lxml import etree
from StringIO import StringIO
from bs4 import BeautifulSoup


def duc_file_parser(filename):
	"""
		filename: file to read. should include the full path
		returns: sentences as a list
	"""
	data = open(filename).read()
	soup = BeautifulSoup(data,"lxml")
	paragraph = soup('text')
	sentences = []
	for text in paragraph:
		lines = text.findAll('s')
		for line in lines:
			sentences.append(line.text.strip())
	return sentences

def read_duc(current_dir="."):
	"""
		current_dir= path to docs.with.sentence.breaks
		returns: dictionary of file and sentences
	"""
	folders = [inode for inode in listdir(current_dir) if not isfile(join(current_dir, inode))]
	dic = {}
	for folder in folders:
		files = [inode for inode in listdir(current_dir+"/"+folder) if isfile(join(current_dir+"/"+folder, inode))]
		for file in files:
			dic[(folder[:-1]+"."+file.split(".")[0]).upper()+"_system_a.txt"] = duc_file_parser(current_dir+"/"+folder+"/"+file)
	return dic

