# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Script to evaluate a skip-thoughts model.

This script can evaluate a model with a unidirectional encoder ("uni-skip" in
the paper); or a model with a bidirectional encoder ("bi-skip"); or the
combination of a model with a unidirectional encoder and a model with a
bidirectional encoder ("combine-skip").

The uni-skip model (if it exists) is specified by the flags
--uni_vocab_file, --uni_embeddings_file, --uni_checkpoint_path.

The bi-skip model (if it exists) is specified by the flags
--bi_vocab_file, --bi_embeddings_path, --bi_checkpoint_path.

The evaluation tasks have different running times. SICK may take 5-10 minutes.
MSRP, TREC and CR may take 20-60 minutes. SUBJ, MPQA and MR may take 2+ hours.
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

import configuration
import encoder_manager
from eval_utility import reader

import string
import numpy as np

FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string("eval_task", "DUC",
                       "Name of the evaluation task to run. Available tasks: "
                       "DUC,CNN-DAILYMAIL")

tf.flags.DEFINE_string("eval_data_dir", None, "Directory containing training data.")

tf.flags.DEFINE_string("uni_vocab_file", None,
                       "Path to vocabulary file containing a list of newline-"
                       "separated words where the word id is the "
                       "corresponding 0-based index in the file.")
tf.flags.DEFINE_string("bi_vocab_file", None,
                       "Path to vocabulary file containing a list of newline-"
                       "separated words where the word id is the "
                       "corresponding 0-based index in the file.")

tf.flags.DEFINE_string("uni_embeddings_file", None,
                       "Path to serialized numpy array of shape "
                       "[vocab_size, embedding_dim].")
tf.flags.DEFINE_string("bi_embeddings_file", None,
                       "Path to serialized numpy array of shape "
                       "[vocab_size, embedding_dim].")

tf.flags.DEFINE_string("uni_checkpoint_path", None,
                       "Checkpoint file or directory containing a checkpoint "
                       "file.")
tf.flags.DEFINE_string("bi_checkpoint_path", None,
                       "Checkpoint file or directory containing a checkpoint "
                       "file.")
tf.flags.DEFINE_boolean("with_attention", False,
                       "Model with attention.")
tf.flags.DEFINE_boolean("with_save_op", False,
                       "Model with attention.")


tf.logging.set_verbosity(tf.logging.INFO)


def eval_duc(encoder, evaltest=True, loc=FLAGS.eval_data_dir):
  data_dict = reader.read_duc(loc)
  remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)
  for key in data_dict.iterkeys():
    # print(len(data_dict[key]))
    encodings2 = encoder.encode(data_dict[key], verbose=True)
    encodings = []
    for i in encodings2[0]:
      # print(i[0])
      encodings.append(i[1])
    top_args = np.argsort(encodings)[::-1]
    # print(top_args)
    idx = []
    word_cnt = 0
    for temp in xrange(len(data_dict[key])):
      cnt = len(data_dict[key][top_args[temp]].translate(remove_punctuation_map).split(" "))
      if word_cnt + cnt > 100:
        break
      idx.append(top_args[temp])
      word_cnt += cnt
    f = open("./evaluation/DUC/DUC_RES/"+key,"w+")
    for kk in idx:
      f.write(data_dict[key][kk]+"\n")
    f.close()



def eval_cnn(encoder, evaltest=True, loc=FLAGS.eval_data_dir):
  pass


def main(unused_argv):
  if not FLAGS.eval_data_dir:
    raise ValueError("--eval_data_dir is required.")

  encoder = encoder_manager.EncoderManager()

  # Maybe load unidirectional encoder.
  if FLAGS.uni_checkpoint_path:
    print("Loading unidirectional model...")
    uni_config = configuration.model_config()
    encoder.load_model(uni_config, FLAGS.uni_vocab_file,
                       FLAGS.uni_embeddings_file, FLAGS.uni_checkpoint_path, FLAGS.with_attention, FLAGS.with_save_op)

  # Maybe load bidirectional encoder.
  if FLAGS.bi_checkpoint_path:
    print("Loading bidirectional model...")
    bi_config = configuration.model_config(bidirectional_encoder=True)
    encoder.load_model(bi_config, FLAGS.bi_vocab_file, FLAGS.bi_embeddings_file,
                       FLAGS.bi_checkpoint_path,FLAGS.with_attention, FLAGS.with_save_op)

  if FLAGS.eval_task == "DUC":
    eval_duc(encoder, evaltest=True, loc=FLAGS.eval_data_dir)
  elif FLAGS.eval_task == "CNN-DAILYMAIL":
    eval_cnn(encoder, evaltest=True, loc=FLAGS.eval_data_dir)
  else:
    raise ValueError("Unrecognized eval_task: %s" % FLAGS.eval_task)
  encoder.close()


if __name__ == "__main__":
  tf.app.run()