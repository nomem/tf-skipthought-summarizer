# Copyright 2017 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Skip-Thoughts model for learning sentence vectors.

The model is based on the paper:

  "Skip-Thought Vectors"
  Ryan Kiros, Yukun Zhu, Ruslan Salakhutdinov, Richard S. Zemel,
  Antonio Torralba, Raquel Urtasun, Sanja Fidler.
  https://papers.nips.cc/paper/5950-skip-thought-vectors.pdf

Layer normalization is applied based on the paper:

  "Layer Normalization"
  Jimmy Lei Ba, Jamie Ryan Kiros, Geoffrey E. Hinton
  https://arxiv.org/abs/1607.06450
"""

# this is a modification of the skipthought training code from tensorflow research models
# Encoder of the skipthought is is fed into an attention model
# Finally a decoder and a dense layer for prediction

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

# import gru cell and input
from ops import gru_cell
from ops import input_ops
# for saving tensors
from tensorflow.python.ops import io_ops

def random_orthonormal_initializer(shape, dtype=tf.float32,
                                   partition_info=None):  # pylint: disable=unused-argument
  """Variable initializer that produces a random orthonormal matrix."""
  if len(shape) != 2 or shape[0] != shape[1]:
    raise ValueError("Expecting square shape, got %s" % shape)
  _, u, _ = tf.svd(tf.random_normal(shape, dtype=dtype), full_matrices=True)
  return u


class SkipthoughtSummarizer(object):
  """Skip-thoughts model."""

  def __init__(self, config, mode="train", with_attention=False, input_reader=None, with_save_op=False, save_op_path="./tmp/intermediate_result.ckpt"):
    """Basic setup. The actual TensorFlow graph is constructed in build().

    Args:
      config: Object containing configuration parameters.
      mode: "train" or "encode".
      input_reader: Subclass of tf.ReaderBase for reading the input serialized
        tf.Example protocol buffers. Defaults to TFRecordReader.

    Raises:
      ValueError: If mode is invalid.
    """
    if mode not in ["train", "encode"]:
      raise ValueError("Unrecognized mode: %s" % mode)

    self.config = config
    self.mode = mode
    self.reader = input_reader if input_reader else tf.TFRecordReader()
    self.with_attention = with_attention
    self.with_save_op = with_save_op
    self.save_op_path = save_op_path

    # Initializer used for non-recurrent weights.
    self.uniform_initializer = tf.random_uniform_initializer(
        minval=-self.config.uniform_init_scale,
        maxval=self.config.uniform_init_scale)

    # Input sentences represented as sequences of word ids. "encode" is the
    # Each is an int64 Tensor with  shape [batch_size, padded_length].
    self.encode_ids = None
    
    # Boolean masks distinguishing real words (1) from padded words (0).
    # Each is an int32 Tensor with shape [batch_size, padded_length].
    self.encode_mask = None

    # Input sentences represented as sequences of word embeddings.
    # Each is a float32 Tensor with shape [batch_size, padded_length, emb_dim].
    self.encode_emb = None
    
    # output label     
    self.label = None
    

    # The output from the sentence encoder.
    # A float32 Tensor with shape [batch_size, max_time_unit,num_gru_units].
    self.thought_vectors = None
    self.summary_classes = None

    # The cross entropy losses and corresponding weights of the decoders. Used
    # for evaluation.
    self.target_cross_entropy_losses = []
    self.target_cross_entropy_loss_weights = []

    # The total loss to optimize.
    self.total_loss = None

    self.word_emb = None

    self.label_emb = None

    self.save_op = None

    self.save_op_objects = []
    self.save_op_names = []


  def build_inputs(self):
    """Builds the ops for reading input data.

    Outputs:
      self.encode_ids
      self.encode_mask
    """
    if self.mode == "encode":
      # Word embeddings are fed from an external vocabulary which has possibly
      # been expanded (see vocabulary_expansion.py).
      encode_ids = None
      encode_mask = tf.placeholder(tf.int8, (None, None), name="encode_mask")
      label = tf.placeholder(tf.int8, (None, 1, 1), name="label")
    else:
      # Prefetch serialized tf.Example protos.
      input_queue = input_ops.prefetch_input_data(
          self.reader,
          self.config.input_file_pattern,
          shuffle=self.config.shuffle_input_data,
          capacity=self.config.input_queue_capacity,
          num_reader_threads=self.config.num_input_reader_threads)

      # Deserialize a batch.
      serialized = input_queue.dequeue_many(self.config.batch_size)
      encode, label = input_ops.parse_example_batch(
          serialized)

      encode_ids = encode.ids
      encode_mask = encode.mask
    
    self.encode_ids = encode_ids
    self.encode_mask = encode_mask
    self.label = label



  def get_save_op(self):
    return self.save_op

  def build_word_embeddings(self):
    """Builds the word embeddings.

    Inputs:
      self.encode_ids

    Outputs:
      self.encode_emb
    """
    if self.mode == "encode":
      # Word embeddings are fed from an external vocabulary which has possibly
      # been expanded (see vocabulary_expansion.py).
      encode_emb = tf.placeholder(tf.float32, (
          None, None, self.config.word_embedding_dim), "encode_emb")
      label_emb = tf.placeholder(tf.float32, (
          None, None, self.config.word_embedding_dim), "label_emb")
      word_emb = None
    else:
      word_emb = tf.get_variable(
          name="word_embedding",
          shape=[self.config.vocab_size, self.config.word_embedding_dim],
          initializer=self.uniform_initializer)
    
      encode_emb = tf.nn.embedding_lookup(word_emb, self.encode_ids)
      label_emb = None

    
    self.encode_emb = encode_emb
    self.word_emb = word_emb
    self.label_emb = label_emb

  def _initialize_gru_cell(self, num_units):
    """Initializes a GRU cell.

    The Variables of the GRU cell are initialized in a way that exactly matches
    the skip-thoughts paper: recurrent weights are initialized from random
    orthonormal matrices and non-recurrent weights are initialized from random
    uniform matrices.

    Args:
      num_units: Number of output units.

    Returns:
      cell: An instance of RNNCell with variable initializers that match the
        skip-thoughts paper.
    """
    return gru_cell.LayerNormGRUCell(
        num_units,
        w_initializer=self.uniform_initializer,
        u_initializer=random_orthonormal_initializer,
        b_initializer=tf.constant_initializer(0.0))

  def build_encoder(self):
    """Builds the sentence encoder.

    Inputs:
      self.encode_emb
      self.encode_mask

    Outputs:
      self.thought_vectors

    Raises:
      ValueError: if config.bidirectional_encoder is True and config.encoder_dim
        is odd.
    """
    with tf.variable_scope("encoder") as scope:
      length = tf.to_int32(tf.reduce_sum(self.encode_mask, 1), name="length")

      if self.config.bidirectional_encoder:
        if self.config.encoder_dim % 2:
          raise ValueError(
              "encoder_dim must be even when using a bidirectional encoder.")
        num_units = self.config.encoder_dim // 2
        cell_fw = self._initialize_gru_cell(num_units)  # Forward encoder
        cell_bw = self._initialize_gru_cell(num_units)  # Backward encoder
        outputs, state = tf.nn.bidirectional_dynamic_rnn(
            cell_fw=cell_fw,
            cell_bw=cell_bw,
            inputs=self.encode_emb,
            sequence_length=length,
            dtype=tf.float32,
            scope=scope)
        if self.with_attention:
            thought_vectors = tf.concat(outputs, 2, name="thought_vectors")
        else:
            thought_vectors = tf.concat(state, 1, name="thought_vectors")
      else:
        cell = self._initialize_gru_cell(self.config.encoder_dim)
        outputs, state = tf.nn.dynamic_rnn(
            cell=cell,
            inputs=self.encode_emb,
            sequence_length=length,
            dtype=tf.float32,
            scope=scope)
        # Use an identity operation to name the Tensor in the Graph.
        if self.with_attention:
            thought_vectors = tf.identity(outputs, name="thought_vectors")
        else:
            thought_vectors = tf.concat(state, 1, name="thought_vectors")
    self.thought_vectors = thought_vectors

  def _build_decoder(self, name, labels, initial_state, reuse_logits):
    """Builds a sentence decoder.

    Args:
      name: Decoder name.
      targets: Batch of target word ids; an int64 Tensor with shape
        [batch_size, padded_length].
      initial_state: Initial state of the GRU. A float32 Tensor with shape
        [batch_size, padded_length, num_gru_cells].
      reuse_logits: Whether to reuse the logits weights.
    """
    # Decoder RNN.
    cell = self._initialize_gru_cell(self.config.encoder_dim)
    with tf.variable_scope(name) as scope:
      # Add a padding word at the start of each sentence (to correspond to the
      # prediction of the first word) and remove the last word.
      decoder_input = None
      if self.mode == "train":
        decoder_input = tf.identity(tf.zeros(labels.shape, dtype=tf.int64))
        # temp_print = tf.Print(decoder_input, [decoder_input], summarize=1000, message="label: ")
        self.label_emb = tf.nn.embedding_lookup(self.word_emb, decoder_input)
      decoder_input = tf.identity(self.label_emb, name="input")
      print(tf.shape(decoder_input))
      
      length = tf.ones(tf.shape(decoder_input)[0], name="length")
      # length = reshape(length, [tf.shape(length)[0],])
      if self.with_attention:
        attention_mechanism = tf.contrib.seq2seq.LuongAttention(self.config.encoder_dim, initial_state, memory_sequence_length=tf.reduce_sum(self.encode_mask, 1, name="source_length"))
        attn_cell = tf.contrib.seq2seq.AttentionWrapper(cell, attention_mechanism, attention_layer_size=self.config.encoder_dim)
        decoder_output, decoder_state = tf.nn.dynamic_rnn(
          cell=attn_cell,
          inputs=decoder_input,
          sequence_length=length,
#           no initial state for now
#           initial_state = decoder_cell.zero_state(batch_size, tf.float32).clone(cell_state=encoder_state),
          dtype=tf.float32,
          scope=scope)
      else:
        decoder_output, decoder_state = tf.nn.dynamic_rnn(
          cell=cell,
          inputs=decoder_input,
          sequence_length=length,
          initial_state=initial_state,
          scope=scope)

    # Stack batch vertically.
    decoder_output = tf.reshape(decoder_output, [-1, self.config.encoder_dim])
    weights = tf.to_float(tf.reshape(length, [-1]))
    # weights = tf.Print(weights, [weights], summarize=1000)

    # Logits.
    with tf.variable_scope("logits", reuse=reuse_logits) as scope:
      logits = tf.contrib.layers.fully_connected(
          inputs=decoder_output,
          num_outputs=2,
          activation_fn=None,
          weights_initializer=self.uniform_initializer,
          scope=scope)
      if self.mode == "encode":
        # summary class
        # self.summary_classes = tf.Print(logits, [logits], name="summary_classes")
        # get max

        logits_indices = tf.argmax(logits, axis=1)
        if self.with_save_op:
          self.save_op_objects.append(logits)
          self.save_op_objects.append(logits_indices)
          self.save_op_names.append("logits")
          self.save_op_names.append("logits_indices")
        # logits = tf.nn.softmax(logits)
        logits = tf.Print(logits, [logits], name="summary_classes", summarize=1000, message="indices: ")
        self.summary_classes = tf.identity(logits, name="summary_indices")


    if self.mode != "encode":
      labels = tf.reshape(labels, [-1])
      onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32), depth=2)
      losses = tf.nn.softmax_cross_entropy_with_logits_v2(
          labels=onehot_labels, logits=logits)
      batch_loss = tf.reduce_sum(losses * weights)
      if self.with_save_op:
        self.save_op_objects.append(losses)
        self.save_op_objects.append(batch_loss)
        self.save_op_names.append("losses")
        self.save_op_names.append("batch_loss")
      # batch_loss = tf.reduce_sum(losses)
      tf.losses.add_loss(batch_loss)

      tf.summary.scalar("losses/" + name, batch_loss)

      self.target_cross_entropy_losses.append(losses)
      self.target_cross_entropy_loss_weights.append(weights)

  def build_decoders(self):
    """Builds the sentence decoders.

    Inputs:
      self.label
      self.thought_vectors

    Outputs:
      self.target_cross_entropy_losses
      self.target_cross_entropy_loss_weights
    """
    # if self.mode != "encode":
      # label
    self._build_decoder("label", self.label, self.thought_vectors, False)


  def build_loss(self):
    """Builds the loss Tensor.

    Outputs:
      self.total_loss
    """
    if self.mode != "encode":
      total_loss = tf.losses.get_total_loss()
      tf.summary.scalar("losses/total", total_loss)
      if self.with_save_op:
        self.save_op_objects.append(total_loss)
        self.save_op_names.append("total_loss")

      self.total_loss = total_loss

  def build_global_step(self):
    """Builds the global step Tensor.

    Outputs:
      self.global_step
    """
    self.global_step = tf.contrib.framework.create_global_step()

  def build(self):
    """Creates all ops for training, evaluation or encoding."""
    self.build_inputs()
    self.build_word_embeddings()
    self.build_encoder()
    self.build_decoders()
    self.build_loss()
    self.build_global_step()
    if self.with_save_op:
      if self.mode == "train":
        self.save_op_objects.extend([self.encode_emb, self.thought_vectors])
        self.save_op_names.extend(["encode_emb", "thought_vectors"])
        self.save_op = io_ops._save(filename=self.save_op_path, tensor_names=self.save_op_names, tensors=self.save_op_objects, name="save_op")
      else:
        self.save_op_objects.extend([self.encode_emb, self.label, self.thought_vectors, self.summary_classes, self.label_emb])
        self.save_op_names.extend(["encode_emb", "label", "thought_vectors", "summary_classes", "label_emb"])
        self.save_op = io_ops._save(filename=self.save_op_path, tensor_names=self.save_op_names, tensors=self.save_op_objects, name="save_op")

