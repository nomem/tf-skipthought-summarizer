import tensorflow as tf
from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file


FLAGS = tf.flags.FLAGS

tf.flags.DEFINE_string("input_file", None, "Input checkpoint file")


def main(unused_arg):
	if not FLAGS.input_file:
		raise ValueError("--input_file is required.")
	print_tensors_in_checkpoint_file(FLAGS.input_file, '', True, True)

	# reader = tf.train.NewCheckpointReader(FLAGS.input_file)
	# a = tf.placeholder(tf.int8, (None, None, None))
	# a = tf.Print(a, [a], message='debug: ', summarize=1000)
	# sess = tf.Session()
	# for tensor in ["label"]:
	# 	sess.run(reader.get_tensor(tensor))


if __name__ == '__main__':
	tf.app.run()